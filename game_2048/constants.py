## constants.py

from typing import Dict

## Default color settings for the tiles
COLORS: Dict[int, Dict[str, str]] = {
    0: {"background": "#ccc0b3", "foreground": "#000000"},
    2: {"background": "#eee4da", "foreground": "#000000"},
    4: {"background": "#ede0c8", "foreground": "#000000"},
    8: {"background": "#f2b179", "foreground": "#ffffff"},
    16: {"background": "#f59563", "foreground": "#ffffff"},
    32: {"background": "#f67c5f", "foreground": "#ffffff"},
    64: {"background": "#f65e3b", "foreground": "#ffffff"},
    128: {"background": "#edcf72", "foreground": "#ffffff"},
    256: {"background": "#edcc61", "foreground": "#ffffff"},
    512: {"background": "#edc850", "foreground": "#ffffff"},
    1024: {"background": "#edc53f", "foreground": "#ffffff"},
    2048: {"background": "#edc22e", "foreground": "#ffffff"},
}

## Default tile size and margin for the board
TILE_SIZE: int = 100
MARGIN: int = 20

## Directions for moving tiles
DIRECTIONS: Dict[str, str] = {
    "UP": "UP",
    "DOWN": "DOWN",
    "LEFT": "LEFT",
    "RIGHT": "RIGHT",
}

## Default win condition and board size for different difficulty levels
DIFFICULTY_LEVELS: Dict[str, Dict[str, int]] = {
    "EASY": {
        "board_size": 4,
        "win_value": 1024,
    },
    "NORMAL": {
        "board_size": 4,
        "win_value": 2048,
    },
    "HARD": {
        "board_size": 5,
        "win_value": 2048,
    },
    "EXPERT": {
        "board_size": 5,
        "win_value": 4096,
    },
}

