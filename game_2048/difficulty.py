## difficulty.py

from typing import Dict
from constants import DIFFICULTY_LEVELS

class Difficulty:
    """
    Class to manage the difficulty level of the game. It handles getting and setting the difficulty level,
    which affects the board size and win conditions.
    """
    
    def __init__(self, level: str = "NORMAL"):
        """
        Initialize the Difficulty class with a default level.
        
        Args:
        level (str): The difficulty level. Default is "NORMAL".
        """
        self._level: str = level
        self._board_size: int = DIFFICULTY_LEVELS[self._level]["board_size"]
        self._win_value: int = DIFFICULTY_LEVELS[self._level]["win_value"]

    def get_difficulty(self) -> str:
        """
        Get the current difficulty level.
        
        Returns:
        str: The current difficulty level.
        """
        return self._level

    def set_difficulty(self, level: str) -> None:
        """
        Set the difficulty level and update the board size and win value accordingly.
        
        Args:
        level (str): The difficulty level to set.
        """
        if level not in DIFFICULTY_LEVELS:
            raise ValueError(f"Invalid difficulty level: {level}. Allowed levels are: {list(DIFFICULTY_LEVELS.keys())}")
        
        self._level = level
        self._board_size = DIFFICULTY_LEVELS[self._level]["board_size"]
        self._win_value = DIFFICULTY_LEVELS[self._level]["win_value"]

    def get_board_size(self) -> int:
        """
        Get the board size for the current difficulty level.
        
        Returns:
        int: The board size.
        """
        return self._board_size

    def get_win_value(self) -> int:
        """
        Get the win value for the current difficulty level.
        
        Returns:
        int: The win value.
        """
        return self._win_value

# Example usage for testing purposes
if __name__ == "__main__":
    difficulty = Difficulty("EASY")
    print(f"Current Difficulty: {difficulty.get_difficulty()}")
    print(f"Board Size: {difficulty.get_board_size()}")
    print(f"Win Value: {difficulty.get_win_value()}")
    
    difficulty.set_difficulty("HARD")
    print(f"Updated Difficulty: {difficulty.get_difficulty()}")
    print(f"Updated Board Size: {difficulty.get_board_size()}")
    print(f"Updated Win Value: {difficulty.get_win_value()}")
